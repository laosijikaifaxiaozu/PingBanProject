package com.open.pingban.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.open.pingban.R;
import com.open.pingban.utils.OPENLOG;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * 播放器窗口.
 */
public class VideoPlayerActivity extends BaseActivity {

//    @InjectView(R.id.video_play_view)
//    VideoView videoPlayView;
    @InjectView(R.id.load_pb)
    ProgressBar loadPb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        ButterKnife.inject(this);
        initVideoPlayerLibs();
        initVideoPlayerView();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void initVideoPlayerLibs() {
//        if (!LibsChecker.checkVitamioLibs(this)) {
//            Log.e("hailongqiu", "检查Vitamio有错误!!!!");
//            return;
//        }
    }

    private void initVideoPlayerView() {
        Intent intent = getIntent();
        if (intent != null) {
            String url = intent.getStringExtra("play_addr");
            if (!TextUtils.isEmpty(url)) {
//                videoPlayView.setVideoLayout(VideoView.VIDEO_LAYOUT_ORIGIN, (float) (16.0 / 9.0));
//                videoPlayView.setMediaController(new MediaController(this));
//                videoPlayView.setVideoURI(Uri.parse(url));
                loadPb.setVisibility(View.VISIBLE);
//                videoPlayView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
//                    @Override
//                    public boolean onError(MediaPlayer mp, int what, int extra) {
//                        OPENLOG.E("what:" + what + " extra:" + extra);
//                        return false;
//                    }
//                });
//                videoPlayView.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
//                    @Override
//                    public void onBufferingUpdate(MediaPlayer mp, int percent) {
//                    }
//                });
//                videoPlayView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                    @Override
//                    public void onPrepared(MediaPlayer mp) {
//                        loadPb.setVisibility(View.GONE);
//                    }
//                });
            }
        }
    }

    @Override
    public void onBackPressed() {
//        videoPlayView.stopPlayback();
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

package com.open.pingban.activity;

import android.app.Application;
import android.util.Log;

import com.open.pingban.utils.Constant;
import com.open.pingban.utils.SharedPreferencesUtils;

public class PingBanApplication extends Application {

    private static PingBanApplication instance;

    public static PingBanApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initRootAddr();
    }

    public void initRootAddr() {
          /* 设置默认服务地址 (http://f117cdh.gnway.cc) */
        Constant.ROOT_ADDR = SharedPreferencesUtils.getString(getApplicationContext(),
                Constant.SERVER_ADDR_KEY,
                Constant.DEFUALT_ADDR);
        Log.d("hailongqiu", "ROOT_ADDR:" + Constant.ROOT_ADDR);
    }

}

package com.open.pingban.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.PixelFormat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;

import com.open.pingban.utils.OPENLOG;

import org.video.stream.CoreLib;

/**
 * 酷莓科技播放器.
 */
public class KuMeiPlayer extends SurfaceView {

    private SurfaceHolder mSurfaceHolder;
    private CoreLib mPlayer = null;

    public KuMeiPlayer(Context context) {
        this(context, null);
    }

    public KuMeiPlayer(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public KuMeiPlayer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void destroy() {
        mPlayer.destroy();
    }

    public CoreLib getPlayer() {
        return this.mPlayer;
    }

    public void resume() {
        mPlayer.resume();
    }

    public void pause() {
        mPlayer.pause();
    }

    public void stop() {
        mPlayer.stop();
    }

    public void setVideoPath(String path) {
        OPENLOG.D("path:" + path);
        init(mActivity);
        this.mPlayer.readMedia(path);
    }

    private Activity mActivity;

    public void init(Activity activity) {
        if (activity != null) {
            mActivity = activity;
            initSurfaceHolder();
            initVideoPlayer();
        }
    }

    private void initVideoPlayer() {
        mPlayer = new CoreLib();
        mPlayer.init();
        //解码方式 0软件解码 1 硬件解码
        mPlayer.setDecodeMode(1);
        //渲染方式 0窗口渲染 1 opengl 渲染
        mPlayer.setRenderMode(1);
    }

    private void initSurfaceHolder() {
            mSurfaceHolder = this.getHolder();
            mSurfaceHolder.setFormat(PixelFormat.RGBX_8888);
            mSurfaceHolder.addCallback(mSurfaceCallback);
    }

    // attach and disattach surface to the lib
    private SurfaceHolder.Callback mSurfaceCallback = new SurfaceHolder.Callback() {
        public void surfaceChanged(SurfaceHolder holder, int format, int width,
                                   int height) {
            Log.d("hailongqiu", "surfaceChanged width:" + width + " height:" + height);
            mPlayer.resizeSurface();
        }

        public void surfaceCreated(SurfaceHolder holder) {
            Log.d("hailongqiu", "surfaceCreated");
            mPlayer.attachSurface(holder.getSurface(), mActivity);
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            Log.d("hailongqiu", "surfaceDestroyed~!");
            mPlayer.destroySurface();
        }
    };

    public void setMediaController() {
    }

    /*enum
    {
        ACPLAYER_NOTHING = 0x1,
                ACPLAYER_OPENING,
                ACPLAYER_BUFFERING,
                ACPLAYER_PLAYING,
                ACPLAYER_PAUSED,
                ACPLAYER_ENDED, //0x6
                PLAYER_STOPPING,
                ACPLAYER_STOPPED,
                ACPLAYER_DESTROYED,
                ACPLAYER_NETBLOCK,
                ACPLAYER_ERROR,
    }player_state_t;
    */
    public void recvMessage(int instance, int arg0, int arg1) {
        OPENLOG.D("状态 =>>>> arg1=" + arg1 + " arg0:" + arg0);
        //arg0不用管
        //这个函数需要立即返回，arg1是播放器的状态 具体如上面的enum所示
        //将消息推送至handler
        //Message msg = mHandler.obtainMessage(PLAYER_STATE);
        //mHandler.sendMessage(msg);
    }

    private static final int SURFACE_4_3 = 0;
    private static final int SURFACE_16_9 = 1;
    private static final int SURFACE_FILL = 2;
    private int mCurrentProportion = SURFACE_16_9;
    private int mVideoWidth;
    private int mVideoHeight;

    private void changeSurfaceSize() {
        Log.d("hailongqiu", "changeSurfaceSize 改适配比例");
        // get screen size
        int dw = getMeasuredWidth(); //this.mActivity.getWindowManager().getDefaultDisplay().getWidth();
        int dh = getMeasuredHeight(); // this.mActivity.getWindowManager().getDefaultDisplay().getHeight();

        // calculate aspect ratio
        double ar = (double) mVideoWidth / (double) mVideoHeight;
        // calculate display aspect ratio
        double dar = (double) dw / (double) dh;

        switch (mCurrentProportion) {
            case SURFACE_4_3:
                ar = 4.0 / 3.0;
                if (dar < ar)
                    dh = (int) (dw / ar);
                else
                    dw = (int) (dh * ar);
                break;
            case SURFACE_16_9:
                ar = 16.0 / 9.0;
                if (dar < ar)
                    dh = (int) (dw / ar);
                else
                    dw = (int) (dh * ar);
                break;
            case SURFACE_FILL:
                break;
        }

        //Toast.makeText(this, "修改播放窗口", Toast.LENGTH_LONG).show();
        mSurfaceHolder.setFixedSize(mVideoWidth, mVideoHeight);
        ViewGroup.LayoutParams lp = this.getLayoutParams();
        lp.width = dw;
        lp.height = dh;
        this.setLayoutParams(lp);
        this.invalidate();
    }

}

package com.open.pingban.presenter;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.open.pingban.mode.CameraInfo;
import com.open.pingban.utils.Constant;

import cz.msebera.android.httpclient.Header;

/**
 */
public class RealTimeMonitoringPresenter implements IMonitoringContract.Presenter {

    /**
     * 获取所有需要切换的摄像头.
     */
    private void getCameraList() {
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        asyncHttpClient.get(Constant.GET_CAMERA_LIST_URL, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String data = new String(responseBody);
                    Gson gson = new Gson();
                    CameraInfo cameraListInfo = gson.fromJson(data, CameraInfo.class);
//                    onSuccessFunction(cameraListInfo.get);
                } catch (Exception e) {
                    e.printStackTrace();
                    onFailureFunction();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                onFailureFunction();
            }
        });
    }

    private void onSuccessFunction(CameraInfo cameraListInfo) {
        String type = cameraListInfo.getType();
        String name = cameraListInfo.getName();
    }

    private void onFailureFunction() {
    }

}

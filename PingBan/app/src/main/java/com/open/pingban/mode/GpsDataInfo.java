package com.open.pingban.mode;

/**
 *  GPS数据.
 */
public class GpsDataInfo {
    float alt;
    float angle;
    String cmd;
    float lat;
    float lng;
    float speed;
    String status;
    String time;

    @Override
    public String toString() {
        return "GpsDataInfo{" +
                "alt=" + alt +
                ", angle=" + angle +
                ", cmd='" + cmd + '\'' +
                ", lat=" + lat +
                ", lng=" + lng +
                ", speed=" + speed +
                ", status='" + status + '\'' +
                ", time='" + time + '\'' +
                '}';
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public float getAlt() {
        return alt;
    }

    public void setAlt(float alt) {
        this.alt = alt;
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}

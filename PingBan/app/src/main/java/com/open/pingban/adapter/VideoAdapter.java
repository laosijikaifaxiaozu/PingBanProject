package com.open.pingban.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.open.pingban.R;
import com.open.pingban.mode.VideoItem;
import com.xgimi.ui.auto.utils.AutoUtils;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class VideoAdapter extends BaseAdapter {

    List<VideoItem> mVideos = new ArrayList<>();

    public VideoAdapter() {

    }

    public void initTestDatas() {
        for (int i = 0; i < 10; i++) {
            VideoItem videoItem = new VideoItem();
            videoItem.setsName("电影1111111111" + i);
            mVideos.add(videoItem);
        }
    }

    public void setVideos(List<VideoItem> videos) {
        mVideos = videos;
        notifyDataSetChanged();
    }

    public void addVideos(List<VideoItem> videos) {
        this.mVideos.addAll(videos);
        notifyDataSetChanged();
    }

    public List<VideoItem> getVideos() {
        return this.mVideos;
    }

    @Override
    public int getCount() {
        return mVideos != null ? mVideos.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mVideos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item_grid_video, parent, false);
            viewHolder = new ViewHolder(convertView);
            AutoUtils.auto(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        VideoItem videoItem = mVideos.get(position);
        String imgUrl = videoItem.getsCoverName();
        String text = videoItem.getsName();
        Glide.with(parent.getContext()).load(imgUrl)
                .placeholder(R.drawable.defualt_bg)
                .error(R.drawable.defualt_bg)
                .into(viewHolder.bgIv);
        viewHolder.titleTv.setText(text);
        return convertView;
    }

    class ViewHolder {
        ImageView bgIv;
        TextView titleTv;

        public ViewHolder(View rootView) {
            bgIv = (ImageView) rootView.findViewById(R.id.video_bg_iv);
            titleTv = (TextView) rootView.findViewById(R.id.video_title_tv);
        }
    }

}

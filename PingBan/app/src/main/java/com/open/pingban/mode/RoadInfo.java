package com.open.pingban.mode;

import java.util.List;

/**
 */
public class RoadInfo {
    int code;
    String errorMessage;
    List<RoadResponse> response;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<RoadResponse> getResponse() {
        return response;
    }

    public void setResponse(List<RoadResponse> response) {
        this.response = response;
    }

    class RoadResponse {
        String sort;
        String name;
        String lat;
        String lng;

        public String getSort() {
            return sort;
        }

        public void setSort(String sort) {
            this.sort = sort;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }
    }
}

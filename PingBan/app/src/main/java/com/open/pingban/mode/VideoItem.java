package com.open.pingban.mode;

/**
 * Video Item 信息.
 */
public class VideoItem {
    String nID;
    String sName;
    String sFileName;
    String sCoverName;
    String sDes;
    String nDeleted;
    String sDur;
    String nDur;
    String sUpdateTime;
    String sPinyin;

    public String getnID() {
        return nID;
    }

    public void setnID(String nID) {
        this.nID = nID;
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public String getsFileName() {
        return sFileName;
    }

    public void setsFileName(String sFileName) {
        this.sFileName = sFileName;
    }

    public String getsCoverName() {
        return sCoverName;
    }

    public void setsCoverName(String sCoverName) {
        this.sCoverName = sCoverName;
    }

    public String getsDes() {
        return sDes;
    }

    public void setsDes(String sDes) {
        this.sDes = sDes;
    }

    public String getnDeleted() {
        return nDeleted;
    }

    public void setnDeleted(String nDeleted) {
        this.nDeleted = nDeleted;
    }

    public String getsDur() {
        return sDur;
    }

    public void setsDur(String sDur) {
        this.sDur = sDur;
    }

    public String getnDur() {
        return nDur;
    }

    public void setnDur(String nDur) {
        this.nDur = nDur;
    }

    public String getsUpdateTime() {
        return sUpdateTime;
    }

    public void setsUpdateTime(String sUpdateTime) {
        this.sUpdateTime = sUpdateTime;
    }

    public String getsPinyin() {
        return sPinyin;
    }

    public void setsPinyin(String sPinyin) {
        this.sPinyin = sPinyin;
    }

    @Override
    public String toString() {
        return "VideoItem{" +
                "nID='" + nID + '\'' +
                ", sName='" + sName + '\'' +
                ", sFileName='" + sFileName + '\'' +
                ", sCoverName='" + sCoverName + '\'' +
                ", sDes='" + sDes + '\'' +
                ", nDeleted='" + nDeleted + '\'' +
                ", sDur='" + sDur + '\'' +
                ", nDur='" + nDur + '\'' +
                ", sUpdateTime='" + sUpdateTime + '\'' +
                ", sPinyin='" + sPinyin + '\'' +
                '}';
    }
}

package com.open.pingban.mode;

import com.open.pingban.view.ShareItem;

import java.util.List;

/**
 */
public class CameraData {
    List<CameraInfo> data;

    public List<CameraInfo> getData() {
        return data;
    }

    public void setData(List<CameraInfo> data) {
        this.data = data;
    }
}

package com.open.pingban.activity;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.xgimi.ui.auto.utils.AutoLayoutHelper;

/**
 */
public class BaseActivity extends Activity {
    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        View view = AutoLayoutHelper.onCreateView(name, context, attrs);
        if (view != null)
            return view;
        return super.onCreateView(name, context, attrs);
    }
}

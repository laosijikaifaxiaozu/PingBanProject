package com.open.pingban.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.open.pingban.R;

import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketException;
import de.tavendo.autobahn.WebSocketHandler;

/**
 * 视频会议界面. (废弃)
 */
public class MeetingFragment extends Fragment {

    public static final String wsUrl = "ws://115.29.193.48:8088";
    public WebSocketConnection mConnection = new WebSocketConnection();

    public static MeetingFragment newInstance(String s) {
        MeetingFragment newFragment = new MeetingFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", s);
        newFragment.setArguments(bundle);
        return newFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_meeting, container, false);
        initService();
        return rootView;
    }

    public void initService() {
        try {
            mConnection.connect(wsUrl, new WebSocketHandler() {

                @Override
                public void onOpen() {
                    super.onOpen();
                }

                @Override
                public void onTextMessage(String payload) {
                    super.onTextMessage(payload);
                }

                @Override
                public void onClose(int code, String reason) {
                    super.onClose(code, reason);
                }

            });
        } catch (WebSocketException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mConnection.isConnected()) {
            mConnection.disconnect();
        }
    }

}

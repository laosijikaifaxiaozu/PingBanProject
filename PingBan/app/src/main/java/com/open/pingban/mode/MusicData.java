package com.open.pingban.mode;

import java.util.List;

/**
 */
public class MusicData {

    /**
     * audios : [{"name":"57_-_Rome_-_The_Ancient_Era","url":"http://f117cdh.gnway.cc/planevod/audio/57_-_Rome_-_The_Ancient_Era.mp3"},{"name":"61_-_Russia_-_The_Ancient_Era","url":"http://f117cdh.gnway.cc/planevod/audio/61_-_Russia_-_The_Ancient_Era.mp3"},{"name":"62_-_Russia_-_The_Medieval_Era","url":"http://f117cdh.gnway.cc/planevod/audio/62_-_Russia_-_The_Medieval_Era.mp3"},{"name":"67_-_Scythia_-_The_Industrial_Era","url":"http://f117cdh.gnway.cc/planevod/audio/67_-_Scythia_-_The_Industrial_Era.mp3"}]
     * updatetime : 2016-11-04 16:01:51
     */

    private String updatetime;
    /**
     * name : 57_-_Rome_-_The_Ancient_Era
     * url : http://f117cdh.gnway.cc/planevod/audio/57_-_Rome_-_The_Ancient_Era.mp3
     */

    private List<AudiosBean> audios;

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public List<AudiosBean> getAudios() {
        return audios;
    }

    public void setAudios(List<AudiosBean> audios) {
        this.audios = audios;
    }

    public static class AudiosBean {
        private String name;
        private String url;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}

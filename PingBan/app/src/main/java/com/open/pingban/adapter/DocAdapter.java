package com.open.pingban.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.open.pingban.R;
import com.open.pingban.mode.DocData;
import com.open.pingban.utils.DocUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 文档 Adapter.
 */
public class DocAdapter extends BaseAdapter {

    List<DocData.DocInfo> mDocList = new ArrayList<>();

    public void testDatas() {
    }

    public void setDocList(List<DocData.DocInfo> docList) {
        if (docList != null) {
            this.mDocList = docList;
            notifyDataSetChanged();
        }
    }

    @Override
    public int getCount() {
        return mDocList != null ? mDocList.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mDocList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item_grid_doc_info, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        DocData.DocInfo docInfo = mDocList.get(position);
        int resID = DocUtils.docName2Icon(docInfo.getUrl());
        viewHolder.doc_icon_iv.setImageResource(resID);
        viewHolder.doc_title_tv.setText(docInfo.getName());
        return convertView;
    }

    static class ViewHolder {
        ImageView doc_icon_iv;
        TextView doc_title_tv;

        public ViewHolder(View rootView) {
            doc_icon_iv = (ImageView) rootView.findViewById(R.id.doc_icon_iv);
            doc_title_tv = (TextView) rootView.findViewById(R.id.doc_title_tv);

        }
    }
}

package com.open.pingban.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.open.pingban.R;
import com.open.pingban.mode.MusicData;
import com.xgimi.ui.auto.utils.AutoUtils;

import java.util.List;

/**
 * 音乐播放文件列表Adapter.
 */
public class MusicAdapter extends BaseAdapter {

    List<MusicData.AudiosBean> mMusicItems;

    public void setMusicItems(List<MusicData.AudiosBean> musicItems) {
        this.mMusicItems = musicItems;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mMusicItems != null ? mMusicItems.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mMusicItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item_list_music_info, null);
            AutoUtils.auto(convertView);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        /* 初始化 歌曲播放列表 */
        MusicData.AudiosBean music = mMusicItems.get(position);
        viewHolder.title.setText(music.getName());
        return convertView;
    }

    class ViewHolder {
        TextView title;
        ViewHolder(View rootView) {
            title = (TextView) rootView.findViewById(R.id.music_name_tv);
        }
    }

}

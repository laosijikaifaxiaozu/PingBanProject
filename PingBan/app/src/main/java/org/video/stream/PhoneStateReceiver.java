/*****************************************************************************
 * PhoneStateReceiver.java
 ****************************************************************************/

package org.video.stream;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

public class PhoneStateReceiver extends BroadcastReceiver {
	public static String TAG = "PhoneStateReceiver";
	
    @Override
    public void onReceive(Context context, Intent intent) {
        String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);

        if (state.equals(TelephonyManager.EXTRA_STATE_RINGING) ||
                state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
            //这里处理界面被盖住的问题
        }
    }
}

package org.video.stream;

import android.app.Activity;
import android.os.Build;
import android.util.Log;
import android.view.Surface;

import com.open.pingban.utils.OPENLOG;

public class CoreLib {
    private static final String TAG = "corelib";

    static {
        try {
            System.loadLibrary("SDL2");
            System.loadLibrary("mmsmpr_comm");
            System.loadLibrary("mmsmpr_play");
            OPENLOG.D("player so load over... ...");
        } catch (UnsatisfiedLinkError ule) {
            OPENLOG.E("Can't load corelib library: " + ule);
            System.exit(1);
        } catch (SecurityException se) {
            OPENLOG.E("Encountered a security issue when loading corelib library: " + se);
            System.exit(1);
        }
    }

    private int mInstance = 0;
    private int mDecode = 0;
    private int mRender = 0;
    private int mEncode = 0;
    private int mCharset = 0;
    private int mWifi = 0;
    private Aout mAout = null;

    public CoreLib() {
        mAout = new Aout();
    }

    public int setDecodeMode(int dec) {
        //0 软解 1 mediacodec硬解
        if (Build.VERSION.SDK_INT < 16) {
            mDecode = 0;
            return 0;
        }
        mDecode = dec;
        if (mDecode < 0)
            mDecode = 0;
        if (mDecode > 1)
            mDecode = 1;

        return 0;
    }

    public int setRenderMode(int render) {
        //0 窗口显示 1 opengl显示
        mRender = render;
        if (mRender < 0)
            mRender = 0;
        if (mRender > 1)
            mRender = 1;

        return 0;
    }

    public int getRenderMode() {
        return mRender;
    }

    public int init() {
        if (mInstance == 0)
            mInstance = init0(1000, false);

        if (mInstance != 0)
            return 0;
        else
            return -1;
    }

    public int destroy() {
        if (mInstance != 0)
            return destroy(mInstance);

        return -1;
    }

    public int getSelf() {
        return mInstance;
    }

    public int attachSurface(Surface surf, Activity activity) {
        if (mInstance != 0)
            return attachSurface(mInstance, surf, activity);

        return -1;
    }

    public int resizeSurface() {
        if (mInstance != 0)
            return resizeSurface(mInstance);

        return -1;
    }

    public int destroySurface() {
        if (mInstance != 0)
            return destroySurface(mInstance);

        return -1;
    }

    public int getSurface() {
        if (mInstance != 0)
            return getSurface(mInstance);

        return 0;
    }

    public int readMedia(String media) {
        if (mInstance != 0)
            return readMedia(mInstance, media);

        return -1;
    }

    public int stop() {
        if (mInstance != 0)
            return stop(mInstance);

        return -1;
    }

    public int getTime() {
        if (mInstance != 0)
            return getTime(mInstance);

        return -1;
    }

    public int getLength() {
        if (mInstance != 0)
            return getLength(mInstance);

        return -1;
    }

    public int getStart() {
        if (mInstance != 0)
            return getStart(mInstance);

        return -1;
    }

    public int seek(int second) {
        if (mInstance != 0)
            return seek(mInstance, second);

        return -1;
    }

    public int getState() {
        if (mInstance != 0)
            return getState(mInstance);

        return -1;
    }

    public int pause() {
        if (mInstance != 0)
            return pause(mInstance);

        return -1;
    }

    public int resume() {
        if (mInstance != 0)
            return resume(mInstance);

        return -1;
    }

    public boolean isPlaying() {
        if (mInstance != 0)
            return (isPlaying(mInstance) == 1);

        return false;
    }

    public boolean isLive() {
        if (mInstance != 0)
            return (isLive(mInstance) == 1);

        return false;
    }

    //音频输出

    /**
     * Open the Java audio output.
     * This function is called by the native code
     */
    public void initAout(int sampleRateInHz, int channels, int samples) {
        //Log.d(TAG, "Opening the java audio output");
        mAout.init(sampleRateInHz, channels, samples);
    }

    /**
     * Play an audio buffer taken from the native code
     * This function is called by the native code
     */
    public void playAudio(byte[] audioData, int bufferSize) {
        mAout.playBuffer(audioData, bufferSize);
    }

    /**
     * Close the Java audio output
     * This function is called by the native code
     */
    public void closeAout() {
        Log.d(TAG, "Closing the java audio output");
        mAout.release();
    }

    //NDK接口
    //基本
    private native int init0(int buffer, boolean jump);

    private native int destroy(int instance);

    //播放窗口
    private native int attachSurface(int instance, Surface surf, Activity activity);

    private native int resizeSurface(int instance);

    private native int destroySurface(int instance);

    private native int getSurface(int instance);

    private native int readMedia(int instance, String media);

    private native int stop(int instance);

    private native int getTime(int instance);

    private native int getLength(int instance);

    private native int getStart(int instance);

    private native int seek(int instance, int second);

    //状态
    private native int getState(int instance);

    private native int pause(int instance);

    private native int resume(int instance);

    private native int isPlaying(int instance);

    private native int isLive(int instance);
}


package com.upyun.playdemo.mode;

/**
 * 来访权限.
 */
public class PowerInfo {
    int code;
    String errorMessage;
    String response; // 权限值.

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}

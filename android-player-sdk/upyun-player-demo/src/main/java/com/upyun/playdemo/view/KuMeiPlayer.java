package com.upyun.playdemo.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * 酷莓科技播放器.
 */
public class KuMeiPlayer extends SurfaceView {

    private SurfaceHolder mSurfaceHolder;
    private Activity mActivity;

    public KuMeiPlayer(Context context) {
        this(context, null);
    }

    public KuMeiPlayer(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public KuMeiPlayer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}

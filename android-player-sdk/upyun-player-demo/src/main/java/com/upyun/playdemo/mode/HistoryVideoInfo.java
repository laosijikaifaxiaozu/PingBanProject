package com.upyun.playdemo.mode;

import java.util.List;

/**
 * 历史回放视频记录数据.
 */
public class HistoryVideoInfo {
    int code;
    String errorMessage;
    List<String> response;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<String> getResponse() {
        return response;
    }

    public void setResponse(List<String> response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "HistoryVideoInfo{" +
                "code=" + code +
                ", errorMessage='" + errorMessage + '\'' +
                ", response=" + response +
                '}';
    }

}

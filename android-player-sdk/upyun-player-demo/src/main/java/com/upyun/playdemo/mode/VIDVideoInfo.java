package com.upyun.playdemo.mode;

/**
 *  VID 视频 信息.
 */
public class VIDVideoInfo {
    int code;
    String errorMessage;
    ResponseInfo response;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ResponseInfo getResponse() {
        return response;
    }

    public void setResponse(ResponseInfo response) {
        this.response = response;
    }

    class ResponseInfo {
        String nID;
        String sName;
        String sFileName;
        String sCoverName;
        String sDes;
        String nDeleted;
        String sDur;
        String nDur;
        String sUpdateTime;
        String sPinyin;

        public String getnID() {
            return nID;
        }

        public void setnID(String nID) {
            this.nID = nID;
        }

        public String getsName() {
            return sName;
        }

        public void setsName(String sName) {
            this.sName = sName;
        }

        public String getsCoverName() {
            return sCoverName;
        }

        public void setsCoverName(String sCoverName) {
            this.sCoverName = sCoverName;
        }

        public String getsFileName() {
            return sFileName;
        }

        public void setsFileName(String sFileName) {
            this.sFileName = sFileName;
        }

        public String getsDes() {
            return sDes;
        }

        public void setsDes(String sDes) {
            this.sDes = sDes;
        }

        public String getnDeleted() {
            return nDeleted;
        }

        public void setnDeleted(String nDeleted) {
            this.nDeleted = nDeleted;
        }

        public String getsDur() {
            return sDur;
        }

        public void setsDur(String sDur) {
            this.sDur = sDur;
        }

        public String getnDur() {
            return nDur;
        }

        public void setnDur(String nDur) {
            this.nDur = nDur;
        }

        public String getsUpdateTime() {
            return sUpdateTime;
        }

        public void setsUpdateTime(String sUpdateTime) {
            this.sUpdateTime = sUpdateTime;
        }

        public String getsPinyin() {
            return sPinyin;
        }

        public void setsPinyin(String sPinyin) {
            this.sPinyin = sPinyin;
        }
    }
}

package com.upyun.playdemo.utils;

/**
 * 常量.
 */
public class Constant {

    public static final String UPDATE_NETWORK_ACTION = "com.pingban.updatenetwork"; // 用于更新界面.
    public static final String UPDATE_NETWORK_KEYWORK = "update-network";
    public static final String SWITCH_IPCAM_TYPE = "ipcam";

    public static final String SERVER_ADDR_KEY = "server_addr";

    public static final String DEFUALT_ADDR = "http://f117cdh.gnway.cc";
    public static String ROOT_ADDR = "";
    public static String VIDEO_ID = "zuoshang"; // 回放需要的ID.

    /**
     * 获取音乐列表.
     */
    public static final String GET_AUDIO_LIST_URL =  "/planevod/audios.json";

    /**
     * 获取所有需要切换的摄像头地址.
     * id ？？ 用于回放历史传入.
     * name 切换的名称
     * type 必须为 ipcam 类型.
     * url 播放地址
     */
    public static final String GET_CAMERA_LIST_URL =  "/planevod/ipcams.json"; // 获取所有需要切换的摄像头地址.

    public static final String GET_DOC_URL =  "/planevod/docs.json"; // 文档

    /**
     * 获取视频列表.
     * keyword 关键字
     * page  页码
     * size 数量
     */
    public static final String VIDEO_URL =  "/planevod/api/index.php/api/video"; //  视频地址.

    /**
     * 具体某一视频.
     * vid  视频ID
     */
    public static final String VID_VIDEO_URL =  "/planevod/api/index.php/api/video"; //
    /**
     * 当前路径.
     */
    public static final String ROAD_VIDEO_URL =  "/planevod/api/index.php/api/road";
    /**
     * 来访权限..
     */
    public static final String POWER_VIDEO_URL =  "/planevod/api/index.php/api/power";

    /**
     * 回放接口
     * ID 为录制ID
     * date 为 时间日期.格式为2016-10-25
     * planevod/api/index.php/api/record/id/1000/date/2015-10-25
     */
    public static final String RECORD_VIDEO_URL =  "/planevod/api/index.php/api/record/id/%s/date/%s";
}

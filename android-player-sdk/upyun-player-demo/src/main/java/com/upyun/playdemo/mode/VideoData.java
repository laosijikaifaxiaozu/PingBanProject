package com.upyun.playdemo.mode;

import java.util.List;

/**
 * 视频列表信息.
 */
public class VideoData {
    int code;
    String errorMessage;
    Response response;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public class Response {
        int total;
        int page;
        List<VideoItem> data;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public List<VideoItem> getData() {
            return data;
        }

        public void setData(List<VideoItem> data) {
            this.data = data;
        }

        @Override
        public String toString() {
            return "Response{" +
                    "total=" + total +
                    ", page=" + page +
                    ", data=" + data +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "VideoData{" +
                "code=" + code +
                ", errorMessage='" + errorMessage + '\'' +
                ", response=" + response +
                '}';
    }
}

package com.upyun.playdemo.mode;

import java.util.List;

/**
 * 文档数据信息.
 */
public class DocData {
    String updatetime;
    List<DocInfo> docs;

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public List<DocInfo> getDocs() {
        return docs;
    }

    public void setDocs(List<DocInfo> docs) {
        this.docs = docs;
    }

    public class DocInfo {
        String name;
        String url;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}

package com.upyun.playdemo.utils;

/**
 * 一些配置常量.
 */
public class Config {
    /** 播放器状态 */
    public static final int PLAYING_STOP = 0;
    public static final int PLAYING_PAUSE = 1;
    public static final int PLAYING_PLAY = 2;
    /** 音乐改变的广播 */
    public static final String RECEIVER_MUSIC_CHANGE = "net.kymjs.music.music_change";

    /** 播放列表循环模式 */
    public static final int MODE_REPEAT_SINGLE = 0;
    public static final int MODE_REPEAT_ALL = 1; // 单曲循环
    public static final int MODE_SEQUENCE = 2; // 列表循环
    public static final int MODE_RANDOM = 3; // 随机循环

}

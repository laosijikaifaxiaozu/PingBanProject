package com.upyun.playdemo.mode;

/**
 * 会议websocket返回的JSON数据.
 */
public class MeetingInfo {
    String cmd;
    int meeting;
    String meeting_address;

    @Override
    public String toString() {
        return "MeetingInfo{" +
                "cmd='" + cmd + '\'' +
                ", meeting=" + meeting +
                ", meeting_address='" + meeting_address + '\'' +
                '}';
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public int getMeeting() {
        return meeting;
    }

    public void setMeeting(int meeting) {
        this.meeting = meeting;
    }

    public String getMeeting_address() {
        return meeting_address;
    }

    public void setMeeting_address(String meeting_address) {
        this.meeting_address = meeting_address;
    }
}

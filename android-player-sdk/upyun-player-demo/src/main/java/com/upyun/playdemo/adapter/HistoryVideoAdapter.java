package com.upyun.playdemo.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.upyun.playdemo.R;
import com.upyun.playdemo.auto.utils.AutoUtils;

import java.util.List;

/**
 */
public class HistoryVideoAdapter extends BaseAdapter {

    List<String> mHistoryItems;

    public void setHistoryItems(List<String> historyItems) {
        this.mHistoryItems = historyItems;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mHistoryItems != null ? mHistoryItems.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mHistoryItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HistoryVideoAdapter.ViewHolder viewHolder = null;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item_list_music_info, null);
            AutoUtils.auto(convertView);
            viewHolder = new HistoryVideoAdapter.ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (HistoryVideoAdapter.ViewHolder) convertView.getTag();
        }
        /* 初始化 历史监控视频列表 */
        String url = mHistoryItems.get(position);
        viewHolder.title.setText(getName(url));
        return convertView;
    }

    private String getName(String url) {
        int last = url.lastIndexOf("/");
        if (last != -1) {
            return url.substring(last + 1);
        }
        return url;
    }

    class ViewHolder {
        TextView title;
        ViewHolder(View rootView) {
            title = (TextView) rootView.findViewById(R.id.music_name_tv);
        }
    }
}

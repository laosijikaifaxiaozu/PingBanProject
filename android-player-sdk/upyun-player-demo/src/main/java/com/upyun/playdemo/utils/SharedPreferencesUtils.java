package com.upyun.playdemo.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * SharedPreferences  工具类.
 */
public class SharedPreferencesUtils {

    private static final String SAVE_XML_NAME = "PingBan";

    public static void putInt(Context context, String name, int value) {
        SharedPreferences.Editor editor = getPreferencesEditor(context);
        editor.putInt(name, value);
        editor.commit();
    }

    public static int getInt(Context context, String name) {
        SharedPreferences sharedPreferences = getPreferences(context);
        return sharedPreferences.getInt("name", 0);
    }

    public static String getString(Context context, String name, String defualtValue) {
        SharedPreferences sharedPreferences = getPreferences(context);
        return sharedPreferences.getString("name", defualtValue);
    }

    public static void putString(Context context, String name, String value) {
        Log.d("hailongqiu", "putString name:" + name + " value:" + value);
        SharedPreferences.Editor editor = getPreferencesEditor(context);
        editor.putString(name, value);
        editor.commit();
    }

    private static SharedPreferences.Editor getPreferencesEditor(Context context) {
        SharedPreferences sharedPreferences = getPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        return editor;
    }

    private static SharedPreferences getPreferences(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SAVE_XML_NAME,
                Activity.MODE_PRIVATE);
        return sharedPreferences;
    }

}

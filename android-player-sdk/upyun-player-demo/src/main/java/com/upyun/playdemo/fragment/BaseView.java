package com.upyun.playdemo.fragment;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

/**
 * 基类 View;
 */
public class BaseView {

    private Activity mActivity;

    public BaseView(Activity activity) {
        this.mActivity = activity;
    }

    public Activity getActivity() {
        return this.mActivity;
    }

    public View onCreateView(LayoutInflater inflater) {
        return null;
    }

    public void onActivityCreated() {

    }

    public void onDestroy() {
    }

    public void startActivity(Intent intent) {
        mActivity.startActivity(intent);
    }

    public void onRefresh() {
    }

    /**
     * 显示提示信息.
     */
    public void showMsg(String msg) {
        try {
            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateNetWorkView() {
    }

    public void stopVideoView() {
    }

}

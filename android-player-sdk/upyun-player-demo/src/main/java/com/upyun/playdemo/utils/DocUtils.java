package com.upyun.playdemo.utils;

import com.upyun.playdemo.R;

/**
 */
public class DocUtils {

    public static int docName2Icon(final String fileName) {
        if (isDoc(fileName)) {
            return R.drawable.ic_doc;
        } else if (isImage(fileName)) {
            return R.drawable.ic_image;
        } else if (fileName.endsWith(".ppt")) {
            return R.drawable.ic_ppt;
        } else if (fileName.endsWith(".pdf")) {
            return R.drawable.ic_pdf;
        }
        return R.drawable.ic_txt;
    }

    public static boolean isImage(String fileName) {
        return fileName.endsWith(".png") || fileName.endsWith(".jpeg") || fileName.endsWith(".gif")
                && fileName.endsWith(".bmp") || fileName.endsWith(".jpg");
    }

    public static boolean isDoc(String fileName) {
        return fileName.endsWith(".doc") || fileName.endsWith(".docx");
    }

}

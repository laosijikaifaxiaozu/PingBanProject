package com.upyun.playdemo.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.upyun.playdemo.R;

/**
 * 用于在主界面显示的工具条的一个item.
 * 监控 回放 视频 音频 会议 设置
 */
public class ItemIconView extends LinearLayout {
    TextView mTitleTv;
    ImageView mIconView;

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    public ItemIconView(Context context) {
        this(context, null);
    }

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    public ItemIconView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setTextSize(int textSize) {
        mTitleTv.setTextSize(textSize);
    }

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    public ItemIconView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        View view = View.inflate(context, R.layout.view_item_icon, this);
        mTitleTv = (TextView) view.findViewById(R.id.title_tv);
        mIconView = (ImageView) view.findViewById(R.id.icon_view);
        if (attrs != null) {
            try {
                TypedArray tArray = context.obtainStyledAttributes(attrs, R.styleable.ItemIconView);// 获取配置属性
                String text = tArray.getString(R.styleable.ItemIconView_title_txt);
                Drawable icon = tArray.getDrawable(R.styleable.ItemIconView_item_icon);
                if (!TextUtils.isEmpty(text)) {
                    mTitleTv.setText(text);
                }
                if (icon != null) {
//                    mIconView.setBackgroundDrawable(icon);
                    setIcon(icon);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setText(String title) {
        mTitleTv.setText(title);
    }

    public void setTextColor(int color) {
        mTitleTv.setTextColor(color);
    }

    public void setIcon(int resID) {
//        mIconView.setBackgroundResource(resID);
        mIconView.setImageResource(resID);
    }

    public void setIcon(Drawable d) {
        mIconView.setImageDrawable(d);
    }
}

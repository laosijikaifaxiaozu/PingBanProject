package com.upyun.playdemo.presenter;

import com.upyun.playdemo.mode.VideoData;

/**
 */
public interface IVideoContract {

    interface IVideoView {
        public void updateVideoList(VideoData videoData);
        public void updateVideoFailure();
    }

    interface IVideoPresenter {
        public void getVideoList(String key, int page, int size);
    }

}

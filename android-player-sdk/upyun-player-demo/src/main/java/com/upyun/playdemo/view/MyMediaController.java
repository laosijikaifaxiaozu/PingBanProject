package com.upyun.playdemo.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.MediaController;

//import io.vov.vitamio.widget.MediaController;

/**
 */
public class MyMediaController extends MediaController {
    public MyMediaController(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyMediaController(Context context) {
        super(context);
    }
}

package com.upyun.playdemo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.MediaController;
import android.widget.ProgressBar;

import com.upyun.playdemo.R;
import com.upyun.upplayer.widget.IRenderView;
import com.upyun.upplayer.widget.UpVideoView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import tv.danmaku.ijk.media.player.IMediaPlayer;

/**
 * 播放器窗口.
 */
public class VideoPlayerActivity extends BaseActivity {

    @InjectView(R.id.load_pb)
    ProgressBar loadPb;
    @InjectView(R.id.upvideo_view)
    UpVideoView upvideoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        ButterKnife.inject(this);
        initVideoPlayerView();
    }

    private void initVideoPlayerView() {
        Intent intent = getIntent();
        if (intent != null) {
            String url = intent.getStringExtra("play_addr");
            if (!TextUtils.isEmpty(url)) {
                upvideoView.fullScreen(this);
                upvideoView.setAspectRatio(IRenderView.AR_16_9_FIT_PARENT);
                upvideoView.setMediaController(new MediaController(this));
                upvideoView.setVideoPath(url);
                upvideoView.start();
                loadPb.setVisibility(View.VISIBLE);
                upvideoView.setOnPreparedListener(new IMediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(IMediaPlayer mp) {
                        loadPb.setVisibility(View.GONE);
                    }
                });
            }
        }
    }

    @Override
    protected void onStop() {
        upvideoView.release(true);
        super.onStop();
    }
}
